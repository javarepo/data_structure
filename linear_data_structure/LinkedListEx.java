package com.test.data_structure.linear_data_structure;

import java.util.LinkedList;  // push() , pop() , add() , remove()

public class LinkedListEx {

	private Node head  ;  // head of list 

	/* Node Class */
	class Node  {

		private int data; 

		private Node next; 

		// Constructor to create a new node 
		Node ( int data )

		{

			this.data = data; 

			this.next = null; 

		}

	}

	public void push(int new_data) { 

		/* 1 & 2: Allocate the Node & 
	              Put in the data*/
		Node new_node = new Node(new_data); 

		/* 3. Make next of new Node as head */
		new_node.next = head; 

		/* 4. Move the head to point to new Node */
		head = new_node; 
	} 

	public void printLinkedList() { 

		Node tempNode = head; 

		while ( tempNode != null ) 

		{ 
			System.out.print( tempNode.data +" " ); 

			tempNode = tempNode.next ; 
		} 
	} 


	public static void main(String args[]) {

		LinkedListEx list = new LinkedListEx();

		list.push(100);

		list.push(200);

		list.printLinkedList();

	}




}

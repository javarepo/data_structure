package com.test.data_structure.linear_data_structure;


import java.util.Stack;

public class  ImplementQueueUsingStacks3 {


	/*private Stack<Integer> q1 = new LinkedList<Integer>();

	private Queue<Integer> q2 = new LinkedList<Integer>();
*/
	private Stack<Integer> s1 = new Stack<Integer>(); 

	private Stack<Integer> s2 = new Stack<Integer>(); 


	private int top;


	public static void main(String arg[]) {


		ImplementQueueUsingStacks3 queueStack = new ImplementQueueUsingStacks3();

		queueStack.add(100);

		queueStack.add(200);

		queueStack.add(300);

		queueStack.add(400);

		queueStack.add(500);

		queueStack.add(600);

		queueStack.add(700);

		queueStack.remove(); 

		// System.out.println(stackQueue.q2);

		System.out.println(queueStack.s1);


	}


	public void add(Integer x) { // Queue add API

		s1.push(x);

		// top = x;
	}

	public void remove() {  // Queue remove API


		while (s1.size() > 1) {

			top = s1.pop();

			s2.push(top);
		}

		s1.pop();

		// Queue<Integer> temp = q1;

		s1 = s2;

	//	q2 = temp;
		
	}

}

package com.test.data_structure.linear_data_structure;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ImplementStackUsingQueues {


	private Queue<Integer> q1 = new LinkedList<Integer>();

	private Queue<Integer> q2 = new LinkedList<Integer>();

	///private Stack<Integer> s1 = new Stack<Integer>(); 

	//private Stack<Integer> s2 = new Stack<Integer>(); 


	private int top;


	public static void main(String arg[]) {


		ImplementStackUsingQueues stackQueue = new ImplementStackUsingQueues();

		stackQueue.push(100);

		stackQueue.push(200);

		stackQueue.push(300);

		stackQueue.push(400);

		stackQueue.push(500);

		stackQueue.push(600);

		stackQueue.push(700);

		stackQueue.pop(); 

		// System.out.println(stackQueue.q2);

		System.out.println(stackQueue.q1);


	}


	public void push(Integer x) {

		q1.add(x);

		// top = x;
	}

	public void pop() {


		while (q1.size() > 2) {

			top = q1.remove();

			q2.add(top);
		}

		q1.remove();

		// Queue<Integer> temp = q1;

		q1 = q2;

	//	q2 = temp;
		
	}

}
